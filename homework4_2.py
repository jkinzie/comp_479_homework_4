from collections import Counter
from html.parser import HTMLParser
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
import sklearn as sk
from sklearn.utils import shuffle
from sklearn.linear_model import LogisticRegression
import math
import string
import time
import re
from itertools import combinations
import sys

from threading import Thread
from threading import Barrier
from multiprocessing.pool import ThreadPool
import multiprocessing as mp


class output_result:
    def __init__(self, value, c, reg):
        self.value = value
        self.c = c
        self.reg = reg # "none" # none, l1, l2


def separator():
    print(50 * '=')
    print(50 * '-')


def get_data(path):
    # open file
    p = open(path, "r")
    # read file into variable
    data = p.read()
    # make some soup
    soup = BeautifulSoup(data, features="lxml")
    # get the table with the data in it
    table = soup.find('table', {'class': 'highlight tab-size js-file-line-container'})

    # return dataframe
    df_return = pd.DataFrame()
    # list of rows
    rows = list()
    # iterate through the table with the td tag
    record = 0
    for row in table.findAll("td"):
        # check if the row has the right blob in the line

        if str(row).__contains__("blob-code"):
            # double split the row to extract the text
            proc_text = (str(row).split('>')[1]).split('<')[0]
            # remove all punctuation
            proc_text = proc_text.translate(str.maketrans('', '', string.punctuation))

            rows.append(proc_text)
            record = record + 1

            print(record)
            if record > CUTOFF:
                break

    # create a dataframe from the rows
    df = pd.DataFrame(rows)
    # shuffle dataframe
    # df = shuffle(df ) # random_state=12345

    return df


def vector(df_text, is_positive):
    print("VECTOR_START")
    row_dataframe_list = list()

    record = 0
    # iterate over the rows in the incoming dataframe
    for index in df_text.index:

        # current row as its own dataframe
        df_current_row = pd.DataFrame()
        # For each index label, access the row contents as series
        row = df_text.loc[index]
        # print row contents
        row_text = row.values[0]

        split_text = row_text.split(sep=' ')

        # trying to remove text we do not want
        split_text = [x for x in split_text if x not in REMOVE_LIST]
        split_text = list(filter(None, split_text))

        # for word in split_text:
        #     word = re.sub(r'\W+', '', word)
        # print(re.sub(r'\W+', '', word))

        # row cols are a set of the current split text
        row_cols = set(split_text)

        # iterate over the word in the list
        for col in row_cols:
            # check if the col exists in the current dataframe for this row
            if col in df_current_row.columns:
                # increment value in dataframe if the col exists
                df_current_row[str(col)] += [1]
            else:
                # if the column doesnt exist, then we set the value of that occurrence to 1
                df_current_row[str(col)] = [1]

        # the current dataframe for this row is now constructed
        # add the new row to the vector
        # df_vector = df_vector.append(df_current_row, sort=True)

        row_dataframe_list.append(df_current_row)

        record = record + 1

        # print(df_vector)

    print("APPENDING DATAFRAMES NOW")
    df_vector = pd.DataFrame()

    if True:
        record = 0
        for i in row_dataframe_list:

            df_vector = df_vector.append(i, sort=True)
            record = record + 1

            if is_positive is 1:
                print("POSITIVE : ", record)
            elif is_positive is -1:
                print("                NEGATIVE : ", record)
    else:
        df_vector = pd.concat(row_dataframe_list, sort=True)

    # convert all nan to zero
    df_vector.fillna(0, inplace=True)

    print("COMPLETED APPENDING DATAFRAMES")
    print(df_vector)

    df_vector = df_vector.reindex(sorted(df_vector.columns), axis=1)

    df_vector.insert(loc=0, column='0000is_positive', value=is_positive)

    print("VECTOR_END!")

    # print(df_vector)
    return df_vector


def Get_Pos():
    positive_text = get_data(POSITIVE)
    return vector(positive_text, 1)


def Get_Neg():
    negative_text = get_data(NEGATIVE)
    return vector(negative_text, -1)


def Get_Vectors():
    pool = ThreadPool(processes=2)
    pos = pool.apply_async(Get_Pos)
    neg = pool.apply_async(Get_Neg)
    return pos.get(), neg.get()


def run_logistic_threaded_pool(in_folds, train_x, train_y, test_x, test_y, output, c_start, c_step, hyper_steps,
                               max_iter):
    loops = in_folds  # - 1
    pool = mp.Pool(processes=3)

    averages = []

    # assign the first c_value
    c_value = c_start
    # regularization
    regularization = ['none','l1','l2']
    # loop to run the hyper-parameters
    for x in range(hyper_steps):
        for reg in regularization:
            print()
            print("RUNNING HYPER STEP : ", hyper_steps)
            print("DEFINING MULTI CORE PROCESSES")
            results = [pool.apply_async(run_logistic_pool_hyper, args=(train_x[x], train_y[x], test_x[x], test_y[x], x, c_value, max_iter, reg)) for x in range(loops)]
            print("RUN MULTI CORE PROCESSES")
            output = [p.get() for p in results]
            print("PRINT MULTI CORE PROCESSES RESULTS")
            print(output)
            print()

            out = output_result((sum(output) / len(output)), c_value, reg)

            averages.append(out)
            print()
            print(" HYPER STEP RESULT : ", hyper_steps)
            print(" THE AVERAGE IS : ", averages[-1].value)
            print(" THE C VALUE IS : ", averages[-1].c)
            print(" THE REG VALUE IS : ", averages[-1].reg)
            print()
            c_value = c_value + c_step

    best = find_highest_output_result(averages)

    separator()
    print()
    print()
    print(" BEST ")
    print(" THE AVERAGE IS : ", best.value)
    print(" THE C VALUE IS : ", best.c)
    print(" THE REG VALUE IS : ", best.reg)

    return best


def find_highest_output_result(in_averages):

    max_perf = 0
    max_out = output_result(0, 0,'none')

    for i in in_averages:
        if max_perf < i.value:
            max_perf = i.value
            max_out = output_result(i.value, i.c, i.reg)
    return max_out


def run_logistic_pool_hyper(train_x, train_y, test_x, test_y, thread_num, c, max_iter, reg):
    print("STARTING THREAD :", thread_num)
    # define the logistic

    logReg = LogisticRegression()
    if 'l1' == reg:
        logReg = LogisticRegression(C=c, solver='liblinear', max_iter=max_iter, penalty=reg)
    else:
        logReg = LogisticRegression(C=c, solver='lbfgs', max_iter=max_iter, penalty=reg)
    # train
    logReg.fit(train_x, train_y.values.ravel())
    # score on test data
    score = logReg.score(test_x, test_y.values.ravel())
    # print(logReg.score(test_x, test_y))
    # return score
    print("ENDING THREAD :", thread_num)
    return score


def run_logistic_with_parameters(train_x, train_y, test_x, test_y, c, max_iter, reg):
    # define the logistic
    logReg = LogisticRegression(C=c, solver='lbfgs', random_state=31415, max_iter=max_iter, penalty=reg)
    # train
    logReg.fit(train_x, train_y.values.ravel())
    # score on test data
    score = logReg.score(test_x, test_y.values.ravel())
    # print(logReg.score(test_x, test_y))
    # return score
    return score


def run_all_combos(in_folds, in_train_x_list, in_train_y_list, c_start, c_step, hyper_steps, max_iter):

    train_x = pd.DataFrame()
    train_y = pd.DataFrame()
    test_x = pd.DataFrame()
    test_y = pd.DataFrame()

    train_x_pool = list()
    train_y_pool = list()
    test_x_pool = list()
    test_y_pool = list()

    print()
    validation_offset = in_folds - 1
    print(validation_offset)

    # get all of the data together
    loops = validation_offset
    while loops >= 0:

        train_x = pd.DataFrame()
        train_y = pd.DataFrame()
        test_x = pd.DataFrame()
        test_y = pd.DataFrame()

        join = validation_offset

        while join >= 0:
            print("JOIN : ", join)
            if join == loops:
                print("TESTING SET : ", join)

                test_x = in_train_x_list[join]
                test_y = in_train_y_list[join]
                print(in_train_y_list[join])
                print(test_y)
            else:
                train_x = pd.concat([train_x, in_train_x_list[join]])
                train_y = pd.concat([train_y, in_train_y_list[join]])

            join = join - 1

        loops = loops - 1

        train_x_pool.append(train_x)
        train_y_pool.append(train_y)
        test_x_pool.append(test_x)
        test_y_pool.append(test_y)

    print()
    print()
    print("MULTI CORE VERSION GO!")

    output = mp.Queue()
    return run_logistic_threaded_pool(N_FOLDS, train_x_pool, train_y_pool, test_x_pool, test_y_pool, output, c_start,
                                      c_step, hyper_steps, max_iter)


######################################################################################


if __name__ == '__main__':
    # positive reviews
    POSITIVE = "rt-polarity.pos"
    # negative reviews
    NEGATIVE = "rt-polarity.neg"
    # training / test split. .8 is 80 % training
    SPLIT = .8
    # number of folds - n folds
    N_FOLDS = 10
    # minimum number of instances to be in the vector
    MIN_INSTANCES = 10
    # maximum number of text rows to process
    CUTOFF = 5999
    # controls if the data is imported directly from the csv or if the data is re-generated from the source text
    USE_CSV = True
    CSV_NAME = "slim_dataset_6000_min_inst10.csv"

    C_START = 1
    C_STEP = .1
    HYPER_STEPS = 100
    MAX_ITER = 10000

    # list of specific sequences to remove
    REMOVE_LIST = [
        "",
        '',
        " ",
        ' ',
        'the',
        'i',
        'us',
        'a',
        'as',
        'an',
        'and',
        'are',
        'be',
        'of',
        'with',
        'at',
        'from',
        'to',
        'in',
        'so',
        'that',
        'this',
        'thats',
        'their',
        'theres',
        'whose',
        'who',
        'what',
        'which',
        'while',
        'than',
        'them',
        'you',
        'youre',
        'your',
        'they',
        'those',
        'for',
        'by',
        'he',
        'his',
        'she',
        'her',
        'do',
        'is',
        'my',
        'it',
        'its',
        'it\'s']

    # full data set
    df_full_dataset = pd.DataFrame()

    # import from csv
    if USE_CSV:
        # read csv from file
        print("READ CSV FROM FILE")
        df_full_dataset = pd.read_csv(CSV_NAME)
        print("CSV FILE READ COMPLETE!")

    # or create the dataset
    else:
        # get the positive and negative data sets with multi-threaded
        df_positive, df_negative = Get_Vectors()

        # combine the positive and negative datasets
        df_full_dataset = df_positive.append(df_negative, sort=True)

        # fill in all NaN as zeros
        df_full_dataset.fillna(0, inplace=True)

        # clear out the csv
        open('full_dataset_' + str(CUTOFF + 1) + '.csv', 'w').close()
        # write the data to the csv
        df_full_dataset.to_csv('full_dataset_' + str(CUTOFF + 1) + '.csv', index=False)

        # remove sparse features
        copy_df_full_dataset = df_full_dataset.copy(deep=True)
        for column in copy_df_full_dataset:
            # don't remove if the row is positive or negative!
            if column != "0000is_positive":
                print()
                print("THE COL IS : ", column)
                # sum the entire col
                sum_column = df_full_dataset[column].sum(axis=0)
                print("THE SUM OF THE COL IS : ", sum_column)
                # if the sum is less than the number of minimum instances, then we drop the table
                if sum_column < MIN_INSTANCES:
                    print("DROPPING : ", column)
                    df_full_dataset.drop(column, axis=1, inplace=True)

        # shuffle data
        # df_full_dataset = df_full_dataset.sample(frac=1).reset_index(drop=True)

        # clear out the csv
        open('full_dataset_' + str(CUTOFF + 1) +'_min_inst'+str(MIN_INSTANCES)+'.csv', 'w').close()
        # write the data to the csv
        df_full_dataset.to_csv('slim_dataset_' + str(CUTOFF + 1) +'_min_inst'+str(MIN_INSTANCES)+'.csv', index=False)

    # break the dataset into x and y
    df_full_dataset_y = df_full_dataset.loc[:, '0000is_positive']
    df_full_dataset_x = df_full_dataset.loc[:, df_full_dataset.columns != '0000is_positive']

    # split the x and y datasets into partitions
    split_datasets_x = np.array_split(df_full_dataset_x, N_FOLDS + 1)
    split_datasets_y = np.array_split(df_full_dataset_y, N_FOLDS + 1)

    # THE SUPER SECRET LAST DATASET THAT WILL NEVER BE TRAINED ON EVER
    final_test_x = split_datasets_x[-1]
    final_test_y = split_datasets_y[-1]

    # REMOVE THE LAST DATA PARTITION BECAUSE IT IS A SECRET!!!!
    split_datasets_x = split_datasets_x[: len(split_datasets_x) - 1]
    split_datasets_y = split_datasets_y[: len(split_datasets_y) - 1]

    final_train_x = pd.concat(split_datasets_x)
    final_train_y = pd.concat(split_datasets_y)

    # run all of the combos on the data
    best = run_all_combos(in_folds=N_FOLDS, in_train_x_list=split_datasets_x, in_train_y_list=split_datasets_y,
                          c_start=C_START, c_step=C_STEP, hyper_steps=HYPER_STEPS, max_iter=MAX_ITER)

    # the final test running the optimized parameters on completely unseen data
    score = run_logistic_with_parameters(c=best.c, max_iter=MAX_ITER, train_x=final_train_x, train_y=final_train_y,
                                         test_x=final_test_x, test_y=final_test_y, reg=best.reg)
    separator()
    print("THE FINAL SCORE ON UNKNOWN NEVER SEEN DATA IS : ", score)
